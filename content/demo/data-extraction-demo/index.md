+++
title = "Data Extraction Demo"
description = "Automation, Business Process Automation, Custom Softwares, Automated Solutions, Data Extraction."
date = "2021-05-13"
template = "non-post-page.html"

[extra]
toc = true
+++

{{ youtube(id="TUAQdP5Uoh4") }}

**A simple workflow demo of data extraction using Python and Prefect.**

This is an example of automating a process that involves downloading stock data from internet and saving it to a spreadsheet.

### Client Requirement
- Visit the financial website.
- Extract the stock information for GOOGLE
- Save the stock data in a spreadsheet.

### Flow

{% center() %}
![Flow Diagram](data_extraction.png)
{% end %}

> **This is just a very simple demonstration of how a data extraction process can be automated.**

> **It doesn't matter if your process is as simple as this one or complex with many unsure steps & calculations, it can be automated.**
